import java.sql.*;

public class Landwirtschaft 
{
	private static Connection c;

	public static void main(String[] args) 
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:landwirtschaft.db");

			clearTables();
			
			createTableLandwirtschaft();
			createTableLandwirtschaftsadresse();
			createTableBesitzer();
			createTableBesitzeradresse();
			createTableTiere();
			createTableBesitzerTiere();
			createTableStall();
			createTableStallTiere();
			
			System.out.println("\ncreated all tables successfully \n");
			
			insertLandwirtschaft(1,"Staller");
			insertStall("Kuhstall", 100, 1999, 650.5);
			insertTier("Kuh", 350, 2014, "Milka", 001);
			insertBesitzer("Horst", "Lechnerter", "Klaus", 1);
			
			System.out.println("\ninserted all objects successfully\n");
			
			anzahlTiere();
			c.close();

		}
		catch(Exception e)
		{
			System.err.println(e.getClass().getName()+": "+ e.getMessage());
			System.exit(0);
		}
	}
	public static void clearTables() throws SQLException
	{
		Statement stmt = c.createStatement();
		String dropLandwirtschaft = "drop table Landwirtschaft";
		String dropLWAdresse = "drop table Landwirtschaftsadresse";
		String dropBesitzer = "drop table Besitzer";
		String dropBAdresse = "drop table Besitzeradresse";
		String dropTiere = "drop table Tiere";
		String dropBesitzerTiere = "drop table BesitzerTiere";
		String dropStall = "drop table Stall";
		String dropStallTiere = "drop table StallTiere";
		stmt.executeUpdate(dropLandwirtschaft);
		stmt.executeUpdate(dropLWAdresse);
		stmt.executeUpdate(dropBesitzer);
		stmt.executeUpdate(dropBAdresse);
		stmt.executeUpdate(dropTiere);
		stmt.executeUpdate(dropBesitzerTiere);
		stmt.executeUpdate(dropStall);
		stmt.executeUpdate(dropStallTiere);
		stmt.close();
	}
	
	
	public static void createTableLandwirtschaft() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableLandwirtschaftSQL=
				"CREATE TABLE if not exists Landwirtschaft" 
				+ "(adressenID	INTEGER	NOT NULL," 
				+ " firmenname 	TEXT	NOT NULL," 
				+ " PRIMARY KEY	(adressenID,firmenname));";

		System.out.println("created table Landwirtschaft successfully");
		stmt.executeUpdate(createtableLandwirtschaftSQL);
		stmt.close();	
	}
	public static void createTableLandwirtschaftsadresse() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableLandwirtschaftsadresseSQL=
				"CREATE TABLE if not exists Landwirtschaftsadresse "
				+ "(plz 		 INTEGER	NOT NULL,"
				+ " ort 		 TEXT		NOT NULL,"
				+ " strasse		 TEXT		NOT NULL,"
				+ " hausNr 		 INTEGER	NOT NULL,"
				+ " firmenname 	 TEXT, "
				+ " adressenID_LW INTEGER,"
				+ " FOREIGN KEY (adressenID_LW, firmenname) references Landwirtschaft (firmenname, adressenID)," 
				+ " PRIMARY KEY (firmenname, adressenID_LW));";

		System.out.println("created table Landwirtschaftsadresse successfully");
		stmt.executeUpdate(createtableLandwirtschaftsadresseSQL);
		stmt.close();

	}
	public static void createTableBesitzer() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableBesitzerSQL=
				"CREATE TABLE if not exists Besitzer "
				+ "(vorname 		TEXT,"
				+ " nachname		TEXT,"
				+ " zweiterVorname 	TEXT 	NULL,"
				+ " adressenID 		INTEGER,"
				+ " adressenID_LW 	INTEGER,"
				+ " firmenname 		TEXT,"
				+ " FOREIGN KEY(adressenID_LW, firmenname) references Landwirtschaft (adressenID, firmenname)," 
				+ " PRIMARY KEY (vorname, nachname, adressenID_LW, firmenname, adressenID));";

		System.out.println("created table Besitzer successfully");
		stmt.executeUpdate(createtableBesitzerSQL);
		stmt.close();
	}	
	public static void createTableBesitzeradresse() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableBesitzeradresseSQL=
				"CREATE TABLE if not exists Besitzeradresse"
				+ "(plz				INTEGER			NOT NULL,"
				+ " ort 			TEXT			NOT NULL,"
				+ " strasse 		TEXT			NOT NULL,"
				+ " hausNr 			INTEGER			NOT NULL,"
				+ " adressenID_B 	INTEGER,"
				+ " vorname 		TEXT,"
				+ " nachname 		TEXT,"
				+ " FOREIGN KEY (adressenID_B, vorname, nachname) references Besitzer (adressenID, vorname, nachname),"
				+ " PRIMARY KEY (adressenID_B, vorname, nachname));";

		System.out.println("created table Besitzeradresse successfully");
		stmt.executeUpdate(createtableBesitzeradresseSQL);
		stmt.close();

	}
	public static void createTableTiere() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableTierSQL =
				"CREATE TABLE if not exists Tiere "
						+ "(art 		TEXT		NOT NULL, "
						+ " gewicht 	REAL		NOT NULL, "
						+ " geburtsjahr INTEGER		NOT NULL, "
						+ " name 		TEXT		NOT NULL, "
						+ " chipNr 		INTEGER 	PRIMARY KEY);";

		System.out.println("created table Tiere successfully");
		stmt.executeUpdate(createtableTierSQL);
		stmt.close();

	}
	public static void createTableBesitzerTiere() throws SQLException
	{
		Statement stmt = c.createStatement();
		
		String createtableBesitzerTiereSQL =
				"CREATE TABLE if not exists BesitzerTiere "
				+ "(chipNr 		INTEGER, "
				+ " vorname 	TEXT, "
				+ " nachname 	TEXT, "
				+ " kauftag 	INTEGER		NOT NULL, "
				+ " kaufmonat 	INTEGER		NOT NULL, "
				+ " kaufjahr 	INTEGER		NOT NULL, "
				+ " FOREIGN KEY (chipNr) references Tiere (chipNr), "
				+ " FOREIGN KEY (vorname, nachname) references Besitzer (vorname, nachname), "
				+ " PRIMARY KEY (chipNr, vorname, nachname));";
				
				
				System.out.println("created table BesitzerTiere successfully");
				stmt.executeUpdate(createtableBesitzerTiereSQL);
				stmt.close();
	}

	public static void createTableStall() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableStallSQL=
				"CREATE TABLE if not exists Stall "
				+ "(bezeichnung 	TEXT	NOT NULL,"
				+ " plaetze 		INTEGER	NOT NULL,"
				+ " baujahr 		INTEGER	NOT NULL,"
				+ " flaeche 		REAL	NOT NULL,"
				+ " adressenID_LW 	INTEGER,"
				+ " firmenname 		TEXT,"
				+ "	besetztePlaetze INTEGER NULL,"
				+ " FOREIGN KEY (adressenID_LW, firmenname) references Landwirtschaft(adressenID, firmenname),"
				+ " PRIMARY KEY (bezeichnung, adressenID_LW, firmenname));";


		System.out.println("created table Stall successfully");
		stmt.executeUpdate(createtableStallSQL);
		stmt.close();

	}
	public static void createTableStallTiere() throws SQLException
	{
		Statement stmt = c.createStatement();

		String createtableStalltiereSQL= 
				"CREATE TABLE if not exists StallTiere "
				+ "(chipNr 			INTEGER,"
				+ " bezeichnung 	TEXT,"
				+ " FOREIGN KEY (chipNr,bezeichnung) references Tiere (chipNr,bezeichnung),"
				+ " PRIMARY KEY (chipNr, bezeichnung));";

		System.out.println("created table Stalltiere successfully");
		stmt.executeUpdate(createtableStalltiereSQL);
		stmt.close();
	}

	public static void insertLandwirtschaft(int adressenID, String firmenname) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertLandwirtschaftSQL = "INSERT INTO Landwirtschaft (adressenID, firmenname) " +
				"VALUES ("+adressenID+",\""+firmenname+"\");"; 
		System.out.println(insertLandwirtschaftSQL);
		stmt.executeUpdate(insertLandwirtschaftSQL);
		System.out.println("inserted a 'Landwirtschafts' - object successfully");
		stmt.close();
	}
	public static void insertLWAdresse(int plz, String ort, String strasse, int hausnr) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertLWAdresseSQL = "INSERT INTO LandwirtschaftsAdresse (plz, ort, strasse, hausnr) " +
				"VALUES ("+plz+",\""+ort+"\",\""+strasse+"\","+hausnr+" );"; 
		System.out.println(insertLWAdresseSQL);
		stmt.executeUpdate(insertLWAdresseSQL);
		System.out.println("inserted a 'Landwirtschaftsadressen' - object successfully");
		stmt.close();
	}
	public static void insertBesitzer(String vorname, String nachname, String zweitervorname, int adressenID) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertBesitzerSQL = "INSERT INTO Besitzer (vorname, nachname, zweitervorname, adressenID) " +
				"VALUES (\""+vorname+"\",\""+nachname+"\",\""+zweitervorname+"\","+adressenID+" );"; 
		System.out.println(insertBesitzerSQL);
		stmt.executeUpdate(insertBesitzerSQL);
		System.out.println("inserted a 'Besitzer' - object successfully");
		stmt.close();
	}
	public static void insertBAdresse(int plz, String ort, String strasse, int hausnr) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertBesitzerAdresseSQL = "INSERT INTO BesitzerAdresse (plz, ort, strasse, hausnr) " +
				"VALUES ("+plz+",\""+ort+"\",\""+strasse+"\","+hausnr+");"; 
		System.out.println(insertBesitzerAdresseSQL);
		stmt.executeUpdate(insertBesitzerAdresseSQL);
		System.out.println("inserted a 'Besitzeradressen' - object successfully");
		stmt.close();
	}
	public static void insertTier(String art, double gewicht, int geburtsjahr, String name, int chipnr) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertTierSQL = "INSERT INTO Tiere (art,gewicht,geburtsjahr,name,chipNr) " +
				"VALUES (\""+art+"\","+gewicht+","+geburtsjahr+",\""+name+"\","+chipnr+");"; 
		System.out.println(insertTierSQL);
		stmt.executeUpdate(insertTierSQL);
		System.out.println("inserted a 'Tier' - object successfully");
		stmt.close();
	}
	public static void insertBesitzerTiere(int kauftag, int kaufmonat, int kaufjahr) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertBesitzerTiereSQL = "INSERT INTO BesitzerTiere (kauftag, kaufmonat, kaufjahr) " +
				"VALUES ("+kauftag+","+kaufmonat+","+kaufjahr+");"; 
		System.out.println(insertBesitzerTiereSQL);
		stmt.executeUpdate(insertBesitzerTiereSQL);
		System.out.println("inserted a 'BesitzerTier' - object successfully");
		stmt.close();
	}
	public static void insertStall(String bezeichnung, int plaetze, int baujahr, double flaeche) throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertStallSQL = "INSERT INTO Stall (bezeichnung, plaetze, baujahr, flaeche) " +
				"VALUES (\""+bezeichnung+"\","+plaetze+","+baujahr+","+flaeche+");";
		System.out.println(insertStallSQL);
		stmt.executeUpdate(insertStallSQL);
		System.out.println("inserted a 'Stall' - object successfully");
		stmt.close();
	}
	public static void insertStallTiere() throws SQLException
	{
		Statement stmt = c.createStatement();
		String insertStallTiereSQL = "INSERT INTO Tiere () " +
				"VALUES ();"; 
		System.out.println(insertStallTiereSQL);
		stmt.executeUpdate(insertStallTiereSQL);
		System.out.println("inserted a 'StallTier' - object successfully");
		stmt.close();
	}
	
	public static int anzahlTiere() throws SQLException{

		Statement stmt = c.createStatement();

		ResultSet rs = stmt.executeQuery( "SELECT geburtsjahr FROM Tiere;" );
		int zaehler=0;
		while ( rs.next() ) {
			int geburtsjahr = rs.getInt("geburtsjahr");
			zaehler++;
		}
		if(zaehler==1)System.out.println("There is "+zaehler+" object in the database 'Tiere'");
		else System.out.println("There are "+zaehler+" objects in the database 'Tiere'");

		rs.close();
		stmt.close();

		return zaehler;
	}



}